# git-commands

| note | command |
| -- | -- |
| current sha | `git rev-parse HEAD` |
| current sha (short) | `git rev-parse --short HEAD` |
| previous tag sha | `git rev-list -1 $(git describe --abbrev=0 --tags --always ${sha}^)` |
| current branch | `git rev-parse --abbrev-ref HEAD` |
| commit datetime | `git log -1 --format=%ai ${sha}` |
| all contributors by email domain | `git shortlog -e -n --no-merges HEAD \| grep "@email-domain"` |
| current contributors by email domain | `git shortlog -e -n --no-merges ${prevSha}..${sha} \| grep "@email-doman"` |
| describe | `git describe --always --tags` |
| current commit message | `git log -1 --no-merges --oneline` |
| current commit message (body) | `git log -1 --pretty=%b` |
| current commit message (subject) | `git log -1 --pretty=%s` |
